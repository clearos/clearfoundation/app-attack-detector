<?php

/**
 * Attack detector log.
 *
 * @category   apps
 * @package    attack-detector
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2016 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/attack_detector/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('network');
$this->lang->load('attack_detector');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(
    lang('network_ip'),
    lang('attack_detector_rule'),
    lang('attack_detector_remaining_timeout'),
);

///////////////////////////////////////////////////////////////////////////////
// Anchors
///////////////////////////////////////////////////////////////////////////////

$anchors = array();

/////////////////////////////////////////////////////////////////////////////////
// Bans
/////////////////////////////////////////////////////////////////////////////////

foreach ($entries as $entry) {
    $ip = $entry['ip'];
    $order_time = $entry['time'];

    $delete=($entry['deleted']) ?'edit':'delete';
    $delete_anchor='anchor_'.$delete;

    $order_ip = "<span style='display: none'>" . sprintf("%032b", ip2long(preg_replace('/\/\d{1,2}/','',$ip))) . "</span>$ip";

    $row['current_delete']=(string)$entry['deleted'];
    $row['action']='/app/attack_detector/bans/delete/'.$entry['ip']."_".$entry['rule'];


    $row['anchors']=button_set(
         array(
            $delete_anchor('/app/attack_detector/bans/'.$delete.'/'.$entry['ip']."_".$entry['rule'],'high',$options),
          )
    );

    $row['details'] = array(
        $order_ip,
        $entry['rule'],
        $order_time
    );

    $rows[] = $row;
}

///////////////////////////////////////////////////////////////////////////////
// Table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'default_rows' => 25,
    'no_action' => FALSE,
    'sort-default-col' => 0,
);

echo summary_table(
     lang('attack_detector_log'),
     $anchors,
     $headers,
     $rows,
     $options
);
