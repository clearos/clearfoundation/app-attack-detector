#!/bin/bash

# This adds a jail.local if one does not exist
if [ ! -e /etc/fail2ban/jail.local ]; then
    touch /etc/fail2ban/jail.local
fi

# Make sure there is a [DEFAULT] section in jail.local
CHECKDEFAULT=$(grep '^\[DEFAULT\]' /etc/fail2ban/jail.local 2>/dev/null)
if [ -z "$CHECKDEFAULT" ]; then
    logger -p local6.notice -t installer 'app-attack-detector - Default section to jail.local'
    echo -e '[DEFAULT]\n' >> /etc/fail2ban/jail.local
fi

# Add in the banaction previously modified in jail.conf
CHECK=$(grep '^banaction =' /etc/fail2ban/jail.local 2>/dev/null)
if [ -z "$CHECK" ]; then
    logger -p local6.notice -t installer 'app-attack-detector - adding banaction to jail.local'
    sed -i -e '/^\[DEFAULT\]/!{p;d;};n;a banaction = iptables-ipset-proto6' /etc/fail2ban/jail.local
fi

# Add in the banaction_allports previously modified in jail.conf
CHECK=$(grep '^banaction_allports =' /etc/fail2ban/jail.local 2>/dev/null)
if [ -z "$CHECK" ]; then
    logger -p local6.notice -t installer 'app-attack-detector - adding banaction_allports to jail.local'
    sed -i -e '/^banaction =/a banaction_allports = iptables-ipset-proto6-allports' /etc/fail2ban/jail.local
fi

# Check if ignoreip exists in jail.local. Do not overwrite if it does!
CHECKLOCAL=$(grep '^ignoreip =' /etc/fail2ban/jail.local 2>/dev/null)
if [ -z "$CHECKLOCAL" ]; then

    #check if ignoreip has been altered in jail.conf and copy it across
    CHECK=$(grep '^ignoreip = 127.0.0.1/8$' /etc/fail2ban/jail.conf 2>/dev/null)
    CHECK2=$(grep '^ignoreip =' /etc/fail2ban/jail.conf 2>/dev/null)
    if [ -z "$CHECK" ] && [ -n "$CHECK2" ]; then

        logger -p local6.notice -t installer 'app-attack-detector - copying ignore-ip to jail.local'
        IGNORE=$(grep '^ignoreip =' /etc/fail2ban/jail.conf | sed 's/\//\\\//g')
        sed -i -e '/^\[DEFAULT\]/!{p;d;};n;a CLEAROSIGNORELINE' /etc/fail2ban/jail.local
        sed -i "s/CLEAROSIGNORELINE/$IGNORE/g" /etc/fail2ban/jail.local
        
        F2BVERSION=$(rpm -q --qf "%{VERSION}\n" fail2ban-server)
        if [ "${F2BVERSION:0:3}" == "0.9" ]; then
            logger -p local6.notice -t installer 'app-attack-detector - resetting ignore-ip in jail.conf'
            sed -i "s/^ignoreip =.*/ignoreip = 127.0.0.1\/8/g" /etc/fail2ban/jail.conf
        fi
        
    fi
    
fi

touch /var/clearos/attack_detector/state/local-initialised
