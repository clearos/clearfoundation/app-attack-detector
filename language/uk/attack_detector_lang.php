<?php

$lang['attack_detector_app_name'] = 'Детектор атак';
$lang['attack_detector_app_description'] = 'Додаток «Детектор атак» сканує вашу систему на предмет помилок аутентифікації в різних типах служб, встановлених у вашій системі. Якщо досягнутий поріг збою, додаток заблокує атакуючу систему.';
$lang['attack_detector_bans'] = 'Заборонені';
$lang['attack_detector_log'] = 'Заборонені';
$lang['attack_detector_remaining_timeout'] = 'Заборонено до';
$lang['attack_detector_rule'] = 'Назва правила';
$lang['attack_detector_rules'] = 'Правила';
$lang['attack_detector_rule_name'] = 'Ім`я';
$lang['attack_detector_rule_invalid'] = 'Правило недійсне.';
$lang['attack_detector_settings'] = 'Налаштування';
$lang['attack_detector_whitelist_lans'] = 'Білий список для LAN(s)';
