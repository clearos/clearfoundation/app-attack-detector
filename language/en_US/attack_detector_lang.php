<?php

$lang['attack_detector_app_name'] = 'Attack Detector';
$lang['attack_detector_app_description'] = 'Attack Detector scans your system for authentication failures across various types of services installed on your system.  If the failure threshold is reached, the app will block the attacking system.';
$lang['attack_detector_bans'] = 'Bans';
$lang['attack_detector_log'] = 'Bans';
$lang['attack_detector_remaining_timeout'] = 'Banned Until';
$lang['attack_detector_rule'] = 'Rule Name';
$lang['attack_detector_rules'] = 'Rules';
$lang['attack_detector_rule_name'] = 'Name';
$lang['attack_detector_rule_invalid'] = 'Rule is invalid.';
$lang['attack_detector_settings'] = 'Settings';
$lang['attack_detector_whitelist_lans'] = 'Whitelist LAN(s)';
